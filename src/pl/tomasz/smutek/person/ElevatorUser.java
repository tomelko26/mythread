package pl.tomasz.smutek.person;

import pl.tomasz.smutek.elevator.Direction;
import pl.tomasz.smutek.floor.Floor;

/**
 * Klasa reprezentujaca użytkownika windy
 */
public class ElevatorUser extends Person {


    public static final int AVAREGE_WEIGH_OF_PERSON_KG = 90;

    /**
     * Czy znajduje się w windzie
     */
    boolean isInElevator = false;

    /**
     * Na które piętro chce jechać
     */
    Floor mWantToGoFloor;

    /**
     * Z którego piętra chce jechać?
     */
    Floor mCurrentFloor;

    /**
     * Konstruktor jednoparametrowy
     *
     * @param aFirstName imię
     */
    public ElevatorUser(String aFirstName) {
        super(aFirstName);
    }

    /**
     * Konstruktor dwuparametrowy
     *
     * @param aFirstName imię
     * @param aName      nazwisko
     */
    public ElevatorUser(String aFirstName, String aName) {
        super(aFirstName, aName);
    }

    /**
     * Metoda ma za zadanie zwrócić informację na które piętro chce użytkownik jechać
     * @return {@code DOWN} - do dołu, {@code UP} - do góry
     */
    public Direction getDirection(){
        Direction result = Direction.DOWN;
        if (mWantToGoFloor.getFloorNo() > mCurrentFloor.getFloorNo()){
            result = Direction.UP;
        }

        return result;
    }

    public Floor getCurrentFloor() {
        return mCurrentFloor;
    }

    public Floor getWantToGoFloor() {
        return mWantToGoFloor;
    }

    public void setInElevator(boolean inElevator) {
        isInElevator = inElevator;
    }

    public void setWantToGoFloor(Floor aWantToGoFloor) {
        this.mWantToGoFloor = aWantToGoFloor;
    }

    public void setCurrentFloor(Floor aCurrentFloor) {
        this.mCurrentFloor = aCurrentFloor;
    }
}