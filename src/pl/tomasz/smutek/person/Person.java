package pl.tomasz.smutek.person;

import pl.tomasz.smutek.address.Address;
import pl.tomasz.smutek.weight.Weight;


/**
 * Klasa reprezentująca osobę
 */
public class Person {

    /**
     * Imię
     */
    protected String mFirstName;

    /**
     * Nazwisko
     */
    protected String mName;

    /**
     * Adres
     */
    private Address mAddress;

    /**
     * Waga
     */
    private Weight mWeight;

    /**
     * Konstruktor dwuparametrowy
     *
     * @param aFirstName imię
     * @param aName nazwisko
     */
    public Person(String aFirstName, String aName) {
        this.mFirstName = aFirstName;
        this.mName = aName;
    }


    /**
     * Konstruktor jednoparametrowy
     *
     * @param aFirstName imię
     */
    public Person(String aFirstName) {
        this.mFirstName = aFirstName;
    }

    @Override
    public String toString() {
        return mFirstName;
    }
}
