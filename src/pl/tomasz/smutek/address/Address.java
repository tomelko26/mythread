package pl.tomasz.smutek.address;

/**
 * Klasa do przechowywania adresu
 */
final public class Address {

    /**
     * Miasto
     */
    private City mCity;

    /**
     * Nazwa ulicy
     */
    private String mStreetName;

    /**
     * Numer budynku
     */
    private int mBlockNo;

    /**
     * Numer mieszkania
     */
    private int mApartmentNo;

    /**
     * Kod pocztowy
     */
    private String mPostalCode;

    /**
     * Konstruktor dwuparametrowy, zakładam że nie ma sensy budowac adresu bez tych dwóch danych
     * @param aCity miasto
     * @param aStreetName ulica
     */
    public Address(City aCity, String aStreetName) {
        mCity = aCity;
        mStreetName = aStreetName;
    }

    public City getCity() {
        return mCity;
    }

    public void setCity(City aCity) {
        this.mCity = aCity;
    }

    public String getStreetName() {
        return mStreetName;
    }

    public void setStreetName(String aStreetName) {
        this.mStreetName = aStreetName;
    }

    public int getBlockNo() {
        return mBlockNo;
    }

    public void setBlockNo(int aBlockNo) {
        this.mBlockNo = aBlockNo;
    }

    public int getApartmentNo() {
        return mApartmentNo;
    }

    public void setApartmentNo(int aApartmentNo) {
        this.mApartmentNo = aApartmentNo;
    }

    public String getPostalCode() {
        return mPostalCode;
    }

    public void setPostalCode(String aPostalCode) {
        this.mPostalCode = aPostalCode;
    }
}
