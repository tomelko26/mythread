package pl.tomasz.smutek.address;

// !uwaga!

/**
 * Enum do przechowywania nazwy miasta
 */
public enum City {

    LUBLIN ("Lublin"), WARSZAWA("Warszawa"), STALOWA_WOLA("Stalowa Wola"), POZNAN("Poznań"), LODZ("Łódź");

    private String mCityName;

    City(String aCityName) {
        mCityName = aCityName;
    }

    public String getCityName() {
        return mCityName;
    }

    @Override
    public String toString() {
        return mCityName;
    }
}
