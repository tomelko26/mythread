package pl.tomasz.smutek.address.utils;

import com.sun.istack.internal.Nullable;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// !uwaga!

/**
 * Klasa narzędziowa służąca do weryfikacji poprawnosci kodu pocztowego,
 * final -  blokuje możliwość dzedziczenia z tej klasy
 */
final public class PostalCodeUtils {

    /**
     * Konstruktor prywatny blokuje możliwość utworzenia obiektu tej klasy spoza klasy
     */
    private PostalCodeUtils() {
        // nic nie robimy
    }

    /**
     * Metoda ma za zadanie sprawdzenie poprawności przekazanego parametrem {@code aCode} kodu pocztowego w zadanym
     * parametrem {@code aRegion} regionie. Jeżeli parametr {@code aRegion} jest {@code null} to region zostanie
     * pobrany z ustawień systemu
     *
     * @param aCode - kod pocztowy
     * @param aRegion - region
     *
     * @return {@code true} jeżeli kod jest poprawny, {@code false} w przeciwnym razie
     */
    public static boolean isCorrect(String aCode, @Nullable String aRegion){

        boolean result = false;

        if (aRegion == null) {
            // !uwaga!
            Locale locale = Locale.getDefault();
            aRegion = locale.getISO3Country();
        }

        // !uwaga!
        Pattern pattern = null;

        switch (aRegion){
            case "POL":
                pattern = Pattern.compile("\\d\\d-\\d\\d\\d");
                break;

            case "USA":
                // do uzupełnienia w domu
                break;

        }

        if (pattern != null) {
            Matcher matcher = pattern.matcher(aCode);
            result = matcher.matches();
        }

        return result;
    }
}
