package pl.tomasz.smutek.block;

import pl.tomasz.smutek.address.Address;
import pl.tomasz.smutek.floor.Floor;

import java.util.List;


/**
 * Klasa reprezentująca blok - abstrakcyjna
 */
public abstract class Block {

    /**
     * Lista pięter w budynku
     */
    protected List<Floor> mFloors;
    /**
     * Adres
     */
    private Address mAddress;

    /**
     * Konstruktor
     *
     * @param aFloors - lista pięter w bloku
     */
    public Block(List<Floor> aFloors) {
        mFloors = aFloors;
    }

    /**
     * Metoda zwracająca informację czy w bloku jest winda
     * @return {@code true} - jeżeli jest winda, {@code false} w przeciwnym razie
     */
    public abstract boolean hasElevator();

    public List<Floor> getFloors() {
        return mFloors;
    }

    public Address getAddress() {
        return mAddress;
    }

    public void setAddress(Address mAddress) {
        this.mAddress = mAddress;
    }
}
