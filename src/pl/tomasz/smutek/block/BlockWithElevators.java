package pl.tomasz.smutek.block;

import pl.tomasz.smutek.elevator.Elevator;
import pl.tomasz.smutek.floor.Floor;
import pl.tomasz.smutek.person.ElevatorUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Klasa reprezentująca blok z windami
 */
public class BlockWithElevators extends Block implements QueueChecker {

    /**
     * Lista dostepych w klasie wind
     */
    private List<Elevator> mElevators;

    /**
     * Koejka do windy na odpowiednim piętrze
     */
    private HashMap<Floor, List<ElevatorUser>> mElevatorQueue;


    public BlockWithElevators(List<Floor> floors) {
        super(floors);

        mElevatorQueue = new HashMap<>();
    }


    @Override
    public boolean hasElevator() {
        return true;
    }

    public List<Elevator> getElevators() {
        return mElevators;
    }

    public void setElevators(List<Elevator> mElevators) {
        this.mElevators = mElevators;
        mElevators.stream().forEach(elevator -> elevator.setQueueChecker(this));
    }

    /**
     * Dodaje użytkownika windy do kolejki
     *
     * @param aFloor piętro na którym zostanie dodany użytkownik
     * @param aElevatorUser uzytkownik który zostanie dodany
     */
    public void addElevatorUserToQueue(Floor aFloor, ElevatorUser aElevatorUser){

        List<ElevatorUser> listOfQueueOnFloor = mElevatorQueue.get(aFloor);

        if (listOfQueueOnFloor == null){
            listOfQueueOnFloor = new ArrayList<>();
            mElevatorQueue.put(aFloor, listOfQueueOnFloor);
        }

        listOfQueueOnFloor.add(aElevatorUser);
    }

    @Override
    public boolean isAnyQueueOnFloor(Floor aFloor) {

        boolean isQueue = false;
        List<ElevatorUser> elevatorUsers = mElevatorQueue.get(aFloor);
        if (elevatorUsers != null && elevatorUsers.size() > 0){
            isQueue = true;
        }
        return isQueue;
    }

    @Override
    public List<ElevatorUser> getQueue(Floor aFloor) {
        return mElevatorQueue.get(aFloor);
    }

    @Override
    public void removePersonFromQueue(ElevatorUser aElevatorUser) {
        List<ElevatorUser> elevatorUsers = mElevatorQueue.get(aElevatorUser.getCurrentFloor());
        elevatorUsers.remove(aElevatorUser);
    }


}
