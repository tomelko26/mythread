package pl.tomasz.smutek.block;

import pl.tomasz.smutek.floor.Floor;
import pl.tomasz.smutek.person.ElevatorUser;

import java.util.List;

/**
 * Interfejs który ma być żródłem danych dla windy o żądaniach jakie sa do obsłużenia
 */
public interface QueueChecker {

    /**
     * Funkcja ma za zadanie zwrócić informację, czy na piętrze zadanym parametrem {@code aFloor} ktos oczekuję
     * @param aFloor - piętro na którym sprawdzamy kolejkę
     * @return {@code true} jeżeli czeka jakakolwiek osoba na tym piętrze, {@code false} w przeciwnym razie
     */
    boolean isAnyQueueOnFloor(Floor aFloor);

    /**
     * Funkcja ma za zadanie zwórcić informację o osobach cozekujących na windę na konkretnym piętrze zadanym parametrem
     * {@code aFloor}
     * @param aFloor - piętro na którym sprawdzamy kolejkę
     * @return Lista osób czekająca na windę, lub {@code null} jeżeli nikt nie oczekuje
     */
    List<ElevatorUser> getQueue(Floor aFloor);

    /**
     * Ma za zadanie usunąć pasażera {@code aElevatorUser} z kolejki
     * @param aElevatorUser - użytkownik który ma zostać usunięty
     */
    void removePersonFromQueue(ElevatorUser aElevatorUser);
}
