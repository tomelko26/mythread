package pl.tomasz.smutek;


import pl.tomasz.smutek.block.BlockWithElevators;
import pl.tomasz.smutek.elevator.Elevator;
import pl.tomasz.smutek.elevator.ElevatorManager;
import pl.tomasz.smutek.elevator.PassengerElevator;
import pl.tomasz.smutek.floor.Floor;
import pl.tomasz.smutek.person.ElevatorUser;
import pl.tomasz.smutek.weight.Weight;
import pl.tomasz.smutek.weight.WeightMetrics;

import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        // Tworzę piętra
        List<Floor> floors = new ArrayList<>(12);
        for (int i = -2; i< 12; i++){
            Floor floor = new Floor(i);
            floors.add(floor);
        }

        // Tworzę blok z pietrami
        BlockWithElevators blockWithElevators = new BlockWithElevators(floors);

        // Tworzę windę, która moze jeździć po wszystkich piętrach
        Elevator elevator = new PassengerElevator();
        elevator.setAvailableFloors(blockWithElevators.getFloors());
        elevator.setCurrentFloor(new Floor(0));
        elevator.setMaxLoad(new Weight(800, WeightMetrics.KG));

        // Dodaję windy do bloku
        blockWithElevators.setElevators(Arrays.asList(elevator));

        // Tworzę obiekt managera i startuje go
        ElevatorManager elevatorManager = new ElevatorManager(blockWithElevators);
        elevatorManager.startWork();

        // W aktualnym wątku nasłuchuje na nowe osoby
        Scanner scanner = new Scanner(System.in);
        String input = "";
        ElevatorUser elevatorUser = null;
        while (true){

            System.out.println("Podaj imię użytkownika.");

            input = scanner.nextLine();

            if (input.compareTo("quit") == 0) {
                break;
            }
            elevatorUser = new ElevatorUser(input);


            System.out.println("Podaj piętro z którego wsiada.");
            input = scanner.nextLine();
            if (input.compareTo("quit") == 0) {
                break;
            }

            int from = 0;
            int i = 0;

                try {
                    from = Integer.valueOf(input);
                } catch (NumberFormatException | NullPointerException ex) {
                    //wyżej mamy dwa błędy złapane z jednym try catch
                    // zostanie wykonany tylko gdy uzyskamy dany błąd
                    //wtedy obsługujemy go w ten sposób
                    System.out.println("podaj obecne piętro raz jeszcze");
                    input = scanner.nextLine();
                    ex.printStackTrace();
                }


            elevatorUser.setCurrentFloor(new Floor(from));



            System.out.println("Podaj piętro na które chce jechać.");
            input = scanner.nextLine();
            if (input.compareTo("quit") == 0) {
                break;
            }

            int to = 0;
            try {
                to = Integer.valueOf(input);
            }catch (NumberFormatException | NullPointerException ex){
                System.out.println("podaj piętro na które chcesz się dostać");
                input = scanner.nextLine();
            }


            elevatorUser.setWantToGoFloor(new Floor(to));

            blockWithElevators.addElevatorUserToQueue(new Floor(from), elevatorUser);
        }

    }
}
