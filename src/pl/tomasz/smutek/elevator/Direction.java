package pl.tomasz.smutek.elevator;


/**
 * Enum definiujący możliwe kierunki poruzania się windy {@code UP} - do góry, {@code DOWN} - do dołu,
 * {@code NONE} - nie zdefiniowany
 */
public enum Direction {
    UP, DOWN, NONE
}
