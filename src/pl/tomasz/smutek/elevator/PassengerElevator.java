package pl.tomasz.smutek.elevator;

import pl.tomasz.smutek.Utils;
import pl.tomasz.smutek.floor.Floor;
import pl.tomasz.smutek.person.ElevatorUser;
import pl.tomasz.smutek.weight.Weight;
import pl.tomasz.smutek.weight.WeightMetrics;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Klasa reprezentująca windę osobową
 */
public class PassengerElevator extends Elevator{

    public static final float SAFETY_WEIGHT_BUFFER = 0.9f;

    /**
     * Lista osób na pokładzie (zamienić na listę generyczną w klasie niżej)
     */
    List<ElevatorUser> mPersonsOnBoard = new ArrayList<>();

    /**
     * Maksymalna liczba osób w windzie
     */
    private short mMaxLoadPerson;

    @Override
    public void setMaxLoad(Weight aMaxLoad) {
        super.setMaxLoad(aMaxLoad);


        float safeWeight = mMaxLoad.getAmount() * SAFETY_WEIGHT_BUFFER;

        float typicalPersonWeight = ElevatorUser.AVAREGE_WEIGH_OF_PERSON_KG;

        if (mMaxLoad.getMetrics() == WeightMetrics.LB){
            typicalPersonWeight *= WeightMetrics.KG_TO_LB_FACTOR;
        }

        mMaxLoadPerson = (short) (safeWeight / typicalPersonWeight);
    }

    @Override
    public void nextStep() {

        // pierwszy przypadek - ludzie wychodzą
        if (anybodyWantExitElevatorOnCurrentFloor()){
            mState = State.EXIT;
            exitFromElevator();
        }
        // drugi przypadek - ludzie wchodza do windy
        else if (anybodyWantEnterElevatorOnCurrentFloor()){
            mState = State.ENTER;
            enterToElevator();
        }
        // trzeci przypadek - winda kogoś przewozi
        else if (anybodyOnBoard()){
            move();
        }
        // winda stoi bez pasażerów - sprawdź czy ktoś czeka na windę
        else if (anybodyRequestElevator()){
            mDirection = calculateDirection();
            // co się stanie jak dojedzie winda do osoby która wybraliśmy? A co się powinno stać?
            move();
        }
        // nic się nie dzieje - odpoczywamy
        else {
            mDirection = Direction.NONE;
            mState = State.STOP;

            if (Utils.SHOW_LOG){
                System.out.println("Winda czeka na piętrze: " + mCurrentFloor + " (brak chętnych do jazdy).");
            }
        }
    }

    private Direction calculateDirection() {
        for (Floor floor : mAvailableFloors) {
            List<ElevatorUser> queue = mQueueChecker.getQueue(floor);
            if (queue != null && !queue.isEmpty()) {
                ElevatorUser elevatorUser = queue.get(0);
                if (elevatorUser.getCurrentFloor().getFloorNo() > mCurrentFloor.getFloorNo()){
                    return Direction.UP;
                } else {
                    return Direction.DOWN;
                }
            }
        }

        return Direction.NONE;
    }

    // zoptymalizować aby szukała najbliżej siebie
    private boolean anybodyRequestElevator() {
        return mAvailableFloors.stream().anyMatch(new Predicate<Floor>() {
            @Override
            public boolean test(Floor floor) {
                List<ElevatorUser> queue = mQueueChecker.getQueue(floor);
                if (queue != null && !queue.isEmpty()){
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    private void move() {
        if(mDirection == Direction.UP){
            getCurrentFloor().upFloor();
        } else {
            getCurrentFloor().downFloor();
        }

        if (Utils.SHOW_LOG){
            System.out.println("Winda jedzie: " + mDirection + " na piętro: " + mCurrentFloor);
        }
    }

    private boolean anybodyOnBoard() {
        return mPersonsOnBoard.size() != 0;
    }

    private void enterToElevator() {
        List<ElevatorUser> queue = mQueueChecker.getQueue(getCurrentFloor());
        int size = queue.size();
        ElevatorUser elevatorUser;
        for(int i = 0; i < size; i++){
            elevatorUser = queue.get(i);
            if(mDirection == Direction.NONE || mPersonsOnBoard.size() == 0){
                mDirection = elevatorUser.getDirection();
            }
            if (elevatorUser.getDirection() == mDirection){
                mPersonsOnBoard.add(elevatorUser);
                mQueueChecker.removePersonFromQueue(elevatorUser);
                elevatorUser.setInElevator(true);
                if (Utils.SHOW_LOG){
                    System.out.println("Użytkownik: " + elevatorUser + " WCHODZI do windy na piętrze: " + mCurrentFloor);
                }
            }
        }
    }

    private boolean anybodyWantEnterElevatorOnCurrentFloor() {
        boolean anybodyGoToElevator = false;
        if (mQueueChecker.isAnyQueueOnFloor(getCurrentFloor())){

            if(mDirection == Direction.NONE || mPersonsOnBoard.size() == 0){
                anybodyGoToElevator = true;
            } else {
                List<ElevatorUser> queue = mQueueChecker.getQueue(getCurrentFloor());
                anybodyGoToElevator = queue.stream().anyMatch(elevatorUser -> elevatorUser.getDirection() == mDirection);
            }
        }

        return anybodyGoToElevator;
    }

    private boolean anybodyWantExitElevatorOnCurrentFloor() {
        return mPersonsOnBoard.stream().anyMatch(elevatorUser -> elevatorUser.getWantToGoFloor().equals(mCurrentFloor));
    }

    private void exitFromElevator() {
        int size = mPersonsOnBoard.size();
        ElevatorUser elevatorUser = null;
        for (int i = size - 1; i >=0; i-- ){
            elevatorUser = mPersonsOnBoard.get(i);
            if (elevatorUser.getWantToGoFloor().equals(mCurrentFloor)){
                mPersonsOnBoard.remove(i);
                elevatorUser.setInElevator(false);
                if (Utils.SHOW_LOG){
                    System.out.println("Użytkownik: " + elevatorUser + " WYCHODZI z windy na piętrze: " + mCurrentFloor);
                }
            }
        }
    }
}
