package pl.tomasz.smutek.elevator;

import pl.tomasz.smutek.block.QueueChecker;
import pl.tomasz.smutek.floor.Floor;
import pl.tomasz.smutek.weight.Weight;

import java.util.List;

/**
 * Klasa reprezentująca windę
 */
public class Elevator {

    /**
     * Lista pięter obsługiwanych przez windę
     */
    protected List<Floor> mAvailableFloors;

    /**
     * Piętro na którym znajduje się winda
     */
    protected Floor mCurrentFloor;

    /**
     * Czy winda jest w trakcie przeglądu - jest niedostępna
     */
    private boolean mServiceBreak = false;

    /**
     * Maksymalny udźwig windy
     */
    protected Weight mMaxLoad;

    /**
     * W którą stronę porusza się winda
     */
    Direction mDirection = Direction.NONE;

    /**
     * W jakim stanie jest winda
     */
    State mState = State.STOP;

    /**
     * Żródło danych o nowych rządaniach, które sa dostępne
     */
    QueueChecker mQueueChecker;


    public List<Floor> getAvailableFloors() {
        return mAvailableFloors;
    }

    public void setAvailableFloors(List<Floor> mAvailableFloors) {
        this.mAvailableFloors = mAvailableFloors;
    }

    public Floor getCurrentFloor() {
        return mCurrentFloor;
    }

    public void setCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }

    public boolean isServiceBreak() {
        return mServiceBreak;
    }

    public void setServiceBreak(boolean mServiceBreak) {
        this.mServiceBreak = mServiceBreak;
    }

    public Weight getMaxLoadKg() {
        return mMaxLoad;
    }

    public void setMaxLoad(Weight aMaxLoad) {
        this.mMaxLoad = aMaxLoad;
    }

    public Direction getDirection() {
        return mDirection;
    }

    public void setDirection(Direction aDirection) {
        this.mDirection = aDirection;
    }

    @Override
    public String toString() {

        StringBuilder msg = new StringBuilder("Jestem: ")
                .append(super.toString())
                .append(" moje piętro: ")
                .append(mCurrentFloor.getFloorNo())
                .append(". Kierunek: ")
                .append(mDirection.toString());

        return msg.toString();
    }

    public void nextStep(){
        // wyrzucić wyjątek
    }

    public void setQueueChecker(QueueChecker aQueueChecker){
        mQueueChecker = aQueueChecker;
    }

}
