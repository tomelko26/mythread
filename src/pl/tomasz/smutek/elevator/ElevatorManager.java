package pl.tomasz.smutek.elevator;


import pl.tomasz.smutek.block.BlockWithElevators;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Zarządca wind w bloku
 */
public class ElevatorManager {


    /**
     * blok z windami
     */
    BlockWithElevators mBlockWithElevators;

    /**
     * Czy zarządca pracuje
     */
    AtomicBoolean isWorking = new AtomicBoolean(false);


    public ElevatorManager(BlockWithElevators aBlockWithElevators) {
        this.mBlockWithElevators = aBlockWithElevators;
    }

    public boolean startWork(){
        if (isWorking.get()){
            return false;
        }

        isWorking.set(true);

        new Thread(() -> {while(isWorking.get()){
            moveElevators();
            printState();
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }}).start();

        return true;
    }

    public void stopWork(){
        isWorking.set(false);
    }

    private void printState() {
        // dopisać
    }

    private void moveElevators() {
        mBlockWithElevators.getElevators().stream().forEach(elevator -> elevator.nextStep());
    }

    public AtomicBoolean isWorking() {
        return isWorking;
    }
}
