package pl.tomasz.smutek.weight;

// dopisać testy jednostkowe na zajęciach
public class Weight {



    int mAmount;
    WeightMetrics mWeightMetrics;

    public Weight(int aAmount, WeightMetrics aWeightMetrics) {
        this.mAmount = aAmount;
        this.mWeightMetrics = aWeightMetrics;
    }

    /**
     * Metoda ma zadanie zwróć wagę zapisaną w obiekcie w podanej metryce
     *
     * @param aWeightMetrics metryka w której chcemy pobrać dane
     * @return zwraca wagę w podanej parametrze aWeightMetrics metryce
     */
    public float getWeight(WeightMetrics aWeightMetrics){

        if(mWeightMetrics == aWeightMetrics){
            return mAmount;
        } else if (mWeightMetrics == WeightMetrics.KG){
            return mAmount * WeightMetrics.KG_TO_LB_FACTOR;
        } else {
            return mAmount / WeightMetrics.KG_TO_LB_FACTOR;
        }
    }

    @Override
    public String toString() {
        return mAmount + " " + mWeightMetrics;
    }

    public float getAmount() {
        return mAmount;
    }

    public WeightMetrics getMetrics() {
        return mWeightMetrics;
    }
}
