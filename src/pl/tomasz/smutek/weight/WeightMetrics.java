package pl.tomasz.smutek.weight;


public enum WeightMetrics {
    KG, LB;

    public static final float KG_TO_LB_FACTOR = 2.2f;
}
