package pl.tomasz.smutek.floor;

/**
 * Klasa reprezenująca piętro
 */
public class Floor {

    int mFloorNo;

    public Floor(int aFloorNo) {
        this.mFloorNo = aFloorNo;
    }

    public int getFloorNo() {
        return mFloorNo;
    }


    public void setFloorNo(int mFloorNo) {
        this.mFloorNo = mFloorNo;
    }

    // !uwaga!
    @Override
    public boolean equals(Object obj) {

        boolean result = false;

        if (obj instanceof Floor){

            Floor temp = (Floor) obj;

            if (mFloorNo == temp.mFloorNo)
                result = true;
        }

        return result;
    }

    // !uwaga!
    @Override
    public int hashCode() {
        return Integer.hashCode(mFloorNo);
    }

    public void upFloor(){
        mFloorNo++;
    }

    public void downFloor() {
        mFloorNo--;
    }

    @Override
    public String toString() {
        return String.valueOf(mFloorNo);
    }
}
